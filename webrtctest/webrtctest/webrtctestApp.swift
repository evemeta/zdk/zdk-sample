//
//  webrtctestApp.swift
//  webrtctest
//
//  Created by Patryk Rogalski on 01/11/2023.
//

import SwiftUI

@main
struct webrtctestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
