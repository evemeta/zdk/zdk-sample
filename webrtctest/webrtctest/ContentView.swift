//
//  ContentView.swift
//  webrtctest
//
//  Created by Patryk Rogalski on 01/11/2023.
//

import SwiftUI
import WebKit
import AVKit

struct ContentView: View {
    private var session = AVAudioSession.sharedInstance()
    @State private var wv = WebView(url: URL(string: "https://demo.zdk.zu.casa/?room=8fcb3c47-74c9-49ea-917c-f8748c3fb848")!)
    @State private var player = AVPlayer(url: URL(string: "https://cdn.zu.casa/transcoder/Aeon_1080p_2000.mp4")!)
    
    var body: some View {
        VStack{
            VideoPlayer(player: player).edgesIgnoringSafeArea(.all).frame(height: 200).onAppear {
                addObserver()
                
            }
            .onDisappear {
                removeObserver()
            }
            wv.edgesIgnoringSafeArea(.all)
        }.onAppear{
            activateSession()
            player.play()
        }
    }
    
    private func activateSession() {
        do {

            try session.setCategory(
                .playback,
                mode: .moviePlayback,
                options: [.mixWithOthers]
            )
        } catch _ {}
        
        do {
            try session.setActive(true)
        } catch _ {}
        
        do {
            try session.overrideOutputAudioPort(.speaker)
        } catch _ {}
    }
    
    private func addObserver() {
       NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime,
                                              object: player.currentItem,
                                              queue: nil) { notification in
           player.seek(to: .zero)
           player.play()
       }
        
        NotificationCenter.default.addObserver(forName: AVAudioSession.interruptionNotification,
                                               object: AVAudioSession.sharedInstance(),
                                               queue: nil) { notification in
            guard let userInfo = notification.userInfo,
                let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
                let type = AVAudioSession.InterruptionType(rawValue: typeValue) else {
                    return
            }


            // Switch over the interruption type.
            switch type {
            case .began:
                // An interruption began. Update the UI as necessary.
                print("interruption start")
                activateSession()
                player.play()
                break
            case .ended:
               // An interruption ended. Resume playback, if appropriate.

                print("interruption end")
                guard let optionsValue = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else { return }
                let options = AVAudioSession.InterruptionOptions(rawValue: optionsValue)
                if options.contains(.shouldResume) {
                    print("interruption end 1")
                    // An interruption ended. Resume playback.
                } else {
                    // An interruption ended. Don't resume playback.
                    print("interruption end 2")
                }
                break
            default: ()
            }
        }
       }
       
    private func removeObserver() {
           NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
       }
}

#Preview {
    ContentView()
}


struct WebView: UIViewRepresentable {
    let request: URLRequest
    let webview: WKWebView
    
    init(url: URL) {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = true
        webConfiguration.mediaTypesRequiringUserActionForPlayback = []
        self.webview = WKWebView(frame: .zero, configuration: webConfiguration)
        self.webview.isInspectable = true
        self.webview.customUserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 17_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.1 Mobile/15E148 Safari/604.1"
        self.request = URLRequest(url: url)
    }
    
    func makeUIView(context: Context) -> WKWebView  {
        return self.webview
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.uiDelegate = context.coordinator
        uiView.load(self.request)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    
    class Coordinator: NSObject, WKUIDelegate {
           var parent: WebView

           init(_ parent: WebView) {
               self.parent = parent
           }

            func webView(_ webView: WKWebView,
                requestMediaCapturePermissionFor
                origin: WKSecurityOrigin,initiatedByFrame
                frame: WKFrameInfo,type: WKMediaCaptureType,
                decisionHandler: @escaping (WKPermissionDecision) -> Void){
                 decisionHandler(.grant)
             }
    }

   
    
}
