ifeq ($(shell uname -s),Linux)
	export OS=LINUX
endif
ifeq ($(shell uname -s),Darwin)
	export OS=MACOS
endif
ifeq ($(findstring microsoft,$(shell uname -a)),microsoft)
	export OS=WINDOWS
endif

ifeq ($(ARCH),)
	export ARCH=$(shell arch)
endif
ifeq ($(filter $(ARCH),x86_64 i386 arm64),$(ARCH))
    export ARCH=amd64
endif

ifeq ($(NAMESPACE),dev)
	export NAME=demo-dev-zu-casa
	export REGION=us-central1
	export PROJECT=zucasa
	export ARTIFACT=gcr.io/zucasa/demo
endif
ifeq ($(NAMESPACE),main)
	export NAME=demo
	export REGION=asia-south1
	export PROJECT=zucasa-development-kit
	export ARTIFACT=asia-south1-docker.pkg.dev/zucasa-development-kit/main/demo
endif

VERSION = $(shell docker images --format "{{.Tag}}" $(ARTIFACT) | grep -Eo '[0-9]{11}' | sort -rn | head -n 1)

init:
	@printf "\033[32m[+]\033[0m %s\n" "Initiating.."
	@sh -c "gcloud auth configure-docker gcr.io,asia-south1-docker.pkg.dev --quiet"
	@printf "\033[32m[+]\033[0m %s\n" "Done"

push:
    ifeq ($(filter $(NAMESPACE),dev main),)
		@printf "\033[31m[-]\033[0m %s\n" "invalid namespace"; exit 1
    endif
    ifeq ($(VERSION),)
		@printf "\033[31m[-]\033[0m %s\n" "missing version"; exit 1
    endif
	@printf "\033[32m[+]\033[0m %s\n" "Pushing service ($(ARTIFACT):$(VERSION))"
	@sh -c "docker push $(ARTIFACT):$(VERSION)"
	@printf "\033[32m[+]\033[0m %s\n" "Done"

build:
    ifeq ($(filter $(NAMESPACE),dev main),)
		@printf "\033[31m[-]\033[0m %s\n" "invalid namespace"; exit 1
    endif
	@printf "\033[32m[+]\033[0m %s\n" "Building service ($(ARTIFACT):$(shell date +'%g%j%H%M%S'))"
	@$(if $(filter $(OS),WINDOWS),cmd.exe /c,sh -c) "docker buildx build --platform linux/$(ARCH) -t $(ARTIFACT):$(shell date +'%g%j%H%M%S') -f backend/go/simple-server/Dockerfile ."
	@printf "\033[32m[+]\033[0m %s\n" "Done"

deploy: push
    ifeq ($(filter $(NAMESPACE),dev main),)
		@printf "\033[31m[-]\033[0m %s\n" "invalid namespace"; exit 1
    endif
    ifeq ($(VERSION),)
		@printf "\033[31m[-]\033[0m %s\n" "missing version"; exit 1
    endif
	@printf "\033[32m[+]\033[0m %s\n" "Deploying service ($(ARTIFACT):$(VERSION))"
	@sh -c "gcloud run deploy $(NAME) --region=$(REGION) --image "$(ARTIFACT):$(VERSION)" --project $(PROJECT)"
	@printf "\033[32m[+]\033[0m %s\n" "Done"