import './App.css';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import React from 'react';
import {Main} from './componets/Main.tsx';

const darkTheme = createTheme({
  palette: {
    mode: 'dark'
  }
});

function App() {


  return <ThemeProvider theme={darkTheme}>
    <CssBaseline/>
    <Main/>
  </ThemeProvider>;
}

export default App;
