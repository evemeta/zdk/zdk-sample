import {IGetter} from '@zdk/core_module';
import {useEffect, useRef, useState} from 'react';

type ExtractType<T> = T extends IGetter<infer VALUE> ? VALUE : never;

// this hook will be added to ZDK react utils in future
export function useGetter<T extends IGetter<any>>(getter: T): ExtractType<T>;
export function useGetter<T extends IGetter<any>>(getter?: T | undefined): ExtractType<T> | undefined;
export function useGetter<T extends IGetter<any>>(getter?: T): ExtractType<T> | undefined {
  const unlisten = useRef<() => void>();
  const [state, setState] = useState<ExtractType<T> | undefined>(getter?.val);

  useEffect(() => {
    if (!getter) return;
    unlisten.current = getter.onAutoChange((v) => {
      setState(v);
    });
    return () => {
      unlisten.current?.();
    };
  }, [getter]);
  return state;
};
