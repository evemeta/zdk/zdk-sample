import {IDevicesManager} from '@zdk/devices_module';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import React from 'react';
import {FormControl, InputLabel, MenuItem, Select} from '@mui/material';
import {useGetter} from '../utils/useGetter.ts';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

export interface DevicesManagerComponentProps {
  devicesManager: IDevicesManager
}

export const DevicesManagerComponent = ({devicesManager}: DevicesManagerComponentProps) => {
  const availableCams = useGetter(devicesManager.data.availableCams);
  const availableMics = useGetter(devicesManager.data.availableMics);
  const selectedCam = useGetter(devicesManager.data.selectedCam);
  const selectedMic = useGetter(devicesManager.data.selectedMic);
  const camStatus = useGetter(devicesManager.data.camStatus);
  const micStatus = useGetter(devicesManager.data.micStatus);

  return <Paper
    elevation={1}
    style={{
      padding: '12px',
      margin: '12px',
    }}
  >
    <h3>Device Manager</h3>
    <div>
      <Typography variant="button">
        Camera Status:&nbsp;
      </Typography>
      {camStatus}
    </div>
    <div>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Selected camera</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          disabled={camStatus !== 'ready'}
          value={selectedCam || ''}
          label="Selected camera"
          onChange={(ev) => devicesManager.actions.setCam(ev.target.value)}
        >
          {availableCams.map(c => <MenuItem key={c.value} itemID={c.value} value={c.value}>{c.label}</MenuItem>)}
        </Select>
      </FormControl>
    </div>
    <div>
      <Typography variant="button">
        Microphone Status:&nbsp;
      </Typography>
      {micStatus}
    </div>
    <div>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Selected microphone</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          disabled={micStatus !== 'ready'}
          id="demo-simple-select"
          value={selectedMic || ''}
          label="Selected microphone"
          onChange={(ev) => devicesManager.actions.setMic(ev.target.value)}
        >
          {availableMics.map(c => <MenuItem key={c.value} itemID={c.value} value={c.value}>{c.label}</MenuItem>)}
        </Select>
      </FormControl>
    </div>
    <div>
    </div>
    <Box mt={3} display={'flex'} justifyContent={'center'} flexWrap={'wrap'}>
      <Button
        onClick={() => devicesManager.actions.turnOnCam()}>
        turn on camera
      </Button>
      <Button
        onClick={() => devicesManager.actions.turnOffCam()}>
        turn off camera
      </Button>
      <Button
        onClick={() => devicesManager.actions.turnOnMic()}>
        turn on microphone
      </Button>
      <Button
        onClick={() => devicesManager.actions.turnOffMic()}>
        turn off microphone
      </Button>
    </Box>
  </Paper>
}
