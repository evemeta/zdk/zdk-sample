import React, {FC, Fragment, useEffect, useState} from 'react';
import {IConference} from '@zdk/conference_module';
import {IDevicesManager} from '@zdk/devices_module';
import {IRoom} from '@zdk/room_module';
import {IUser} from '@zdk/user_module';
import {useGetter} from '../utils/useGetter.ts';
import {MediaElement} from './VideoElement.tsx';
import {VideocamOffOutlined, VolumeDown, VolumeUp} from '@mui/icons-material';
import Stack from '@mui/material/Stack';
import Card from '@mui/material/Card';
import CircularProgress from '@mui/material/CircularProgress';
import CardContent from '@mui/material/CardContent';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import {AudioElement} from './AudioElement.tsx';
import Button from '@mui/material/Button';
import {Chip, ChipProps, Slider} from '@mui/material';


export const ConferenceComponent: FC<{ conference: IConference, room: IRoom, devicesManager: IDevicesManager, user: IUser }> = ({
  conference,
  room,
  user
}) => {
  const roomUsers = useGetter(conference.data.membersWithMedia);
  const roomStatus = useGetter(room.data.status);
  const roomCapacity = useGetter(room.data.capacity);
  const mainVolume = useGetter(conference.data.mainVolume);
  const clientId = useGetter(user.data.clientId);
  const [color, setColor] = useState<ChipProps['color']>('default');

  useEffect(() => {
    const unlisten = room.events.onCustomRoomPacket(packet => {
      if(packet.name === 'button.change') {
        setColor(packet.data.color as ChipProps['color'])
      }
    })

   return () => unlisten();
  }, [])

  const renderVideo = (video: typeof roomUsers[0]['video']) => {

    if (video.status === 'loading') {
      return <CircularProgress/>;
    }

    if (video.status === 'ready' && video.stream) {
      return <MediaElement mediaStream={video.stream}/>;
    }

    return <VideocamOffOutlined/>;
  };

  const renderRoomUser = (user: typeof roomUsers[0]) => {
    return (<Fragment key={user.member.userId}>
      <Paper
        elevation={3}
        style={{marginBottom: 6}}
      >
        <Card
          variant={clientId === user.member.userId ? 'elevation' : 'outlined'}
          sx={{display: 'flex', flexDirection: 'row'}}
        >
          <CardContent
            sx={{
              display: 'flex',
              alignItems: 'center',
              pl: 1,
              pb: 1,
              width: '100px',
              height: '100px',
              justifyContent: 'center',
              minWidth: '100px'
            }}
          >
            {renderVideo(user.video)}
          </CardContent>
          <div>
            {user.member.userId !== clientId && <AudioElement key={user.audio.stream?.id}
              volume={(user.audio.volume/100)*(mainVolume/100)}
              mediaStream={user.audio.stream}
              muted={user.audio.status !== 'ready'} />}
          </div>
          <CardContent sx={{flex: '1 0 auto'}}>
            <Stack
              direction="column"
              spacing={1}
              mt={0}
            >
              <Typography
                sx={{fontSize: 12}}
                color="text.disabled"
                gutterBottom
              >
                User ID: {user.member.userId}
              </Typography>
              <Typography
                sx={{fontSize: 12}}
                color="text.disabled"
                gutterBottom
              >
                Video status: {user.video.status}
              </Typography>
              <Typography
                sx={{fontSize: 12}}
                color="text.disabled"
                gutterBottom
              >
                Audio status: {user.audio.status}
              </Typography>
            </Stack>
          </CardContent>
          <CardContent sx={{flex: '1 0 auto'}}>
            <Stack
              direction="row"
              justifyContent={'space-between'}
              spacing={4}
              mt={1}
            >
              {user.member.userId !== clientId &&
                <Stack
                  direction="row"
                  spacing={4}
                >
                  <Button onClick={() => conference.actions.toggleUserVideo(user.member.userId)}>Toggle user video</Button>
                  <Button onClick={() => conference.actions.toggleUserAudio(user.member.userId)}>Toggle user audio</Button>
                  <Button onClick={() => room.actions.kickMember(user.member.userId, '')}>Kick user</Button>
                </Stack>
              }
            </Stack>

          </CardContent>

        </Card>
      </Paper>
    </Fragment>);
  };

  return <Paper
    elevation={1}
    style={{
      padding: '12px',
      margin: '12px',
      background: (roomStatus === 'loading' ? '#ffff0021' : undefined)
    }}
  >
    <h3>Room</h3>
    <div>
      <Typography variant="button">
        Room Status:&nbsp;
      </Typography>
      {roomStatus}
    </div>
    <div>
      <Typography variant="button">
        Room Capacity:&nbsp;
      </Typography>
      {Number(roomCapacity)}
    </div>
    <div>
      <Typography variant="button">
        Users Amount:&nbsp;
      </Typography>
      {roomUsers.length}
    </div>
    <Stack spacing={2} direction="row" sx={{ mb: 1 }} alignItems="center">
      <VolumeDown />
      <Slider aria-label="Volume" defaultValue={mainVolume} onChange={(e, v) => {
        if(!Number.isNaN(v)) conference.actions.setMainVolume(v as number)}}/>
      <VolumeUp />
    </Stack>
    <div>
      {roomStatus === 'ready' && roomUsers.map(u => renderRoomUser(u))}
    </div>
    <div>
      <Typography variant="button">
        Custom packet:&nbsp;
      </Typography>
      <Paper
        elevation={3}
        style={{marginBottom: 6}}
      >
        <Card
          variant={'outlined'}
          sx={{display: 'flex', flexDirection: 'row'}}
        >
          <CardContent sx={{flex: '1 0 auto'}}>
            <Stack
              direction="row"
              justifyContent={'center'}
              spacing={4}
              mt={1}
            >
              <Chip label="editable chip" color={color} />
            </Stack>
            <Stack
              direction="row"
              justifyContent={'center'}
              spacing={4}
              mt={1}
            >

                <Stack
                  direction="row"
                  spacing={4}
                >
                  <Button onClick={() => room.actions.sendCustomRoomPacket( 'button.change', { color: 'error' })}>Change to red</Button>
                  <Button onClick={() => room.actions.sendCustomRoomPacket( 'button.change', { color: 'info' })}>Change to blue</Button>
                  <Button onClick={() => room.actions.sendCustomRoomPacket( 'button.change', { color: 'success' })}>Change to green</Button>
                  <Button onClick={() => room.actions.sendCustomRoomPacket( 'button.change', { color: 'warning' })}>Change to yellow</Button>
                  <Button onClick={() => room.actions.sendCustomRoomPacket( 'button.change', { color: 'default' })}>Change to grey</Button>
                </Stack>
            </Stack>

          </CardContent>

        </Card>
      </Paper>
    </div>
  </Paper>;
};
