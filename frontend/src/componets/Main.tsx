import React, {FC, useEffect, useState} from 'react';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import CardContent from '@mui/material/CardContent';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import PanoramaFishEyeIcon from '@mui/icons-material/PanoramaFishEye';

import {RoomChat} from './RoomChat.tsx';
import {ConferenceComponent} from './ConferenceComponent.tsx';
import {DevicesManagerComponent} from './DevicesManagerComponent.tsx';
import {FormControl, Input, InputLabel} from '@mui/material';

import {createWebSocket, WebSocketReconnection, IWebSocket} from '@zdk/web_socket_module';
import {createConference, IConference} from '@zdk/conference_module';
import {createDevicesManager, IDevicesManager} from '@zdk/devices_module';
import {createRoom, IRoom} from '@zdk/room_module';
import {createUser, IUser} from '@zdk/user_module';
import {createChats, IChats} from '@zdk/chats_module';
import {createRoomChat, IRoomChat} from '@zdk/room_chat_module';
import {InstanceDestroyedError, IReporter} from '@zdk/core_module';
import {Reporter} from '@zdk/reporter_module';

import {useGetter} from '../utils/useGetter.ts';


interface ModulesState {
  user: IUser,
  chats: IChats,
  webSocket: IWebSocket,
  roomChat: IRoomChat,
  devicesManager?: IDevicesManager,
  room: IRoom,
  conference?: IConference,
  reporter: IReporter
}

function getCustomBitrate(): number | null {
  const currentUrl = window.location.href;
  let url = new URL(currentUrl);
  let params = new URLSearchParams(url.search);
  return params.has('bitrate') ? parseInt(params.get("bitrate")!) : null;
}


function convertBigInts(input: any, seen = new WeakSet()): any {
  if (typeof input === 'bigint') {
    return String(input);
  } else if (Array.isArray(input)) {
    return input.map(item => convertBigInts(item, seen));
  } else if (input && typeof input === 'object') {
    if (seen.has(input)) {
      return '[Circular]';
    }
    seen.add(input);

    const result: any = {};
    for (const key of Object.keys(input)) {
      result[key] = convertBigInts(input[key], seen);
    }
    return result;
  }
  return input;
}

let intervalId: ReturnType<typeof setInterval>;

const setRandomInterval = (callback: () => void, minDelay: number, maxDelay: number) => {
  const randInterval = Math.floor(Math.random() * (maxDelay - minDelay + 1)) + minDelay;

  intervalId = setTimeout(() => {
    callback();
    // Clear the current timeout and set the next one
    clearTimeout(intervalId);
    setRandomInterval(callback, minDelay, maxDelay);
  }, randInterval);
};


export const Main: FC = () => {
  const [token, setToken] = useState<string>('');
  const [roomId, setRoomId] = useState<string>('');
  const [autosend, setAutosend] = useState(false);
  const [modules, setModules] = useState<ModulesState | undefined>();

  const userModuleStatus = useGetter(modules?.user.data.status);
  const webSocketModuleStatus = useGetter(modules?.webSocket.data.status);
  const roomModuleStatus = useGetter(modules?.room.data.status);

  useEffect(() => {
    const user = createUser({
      config: {host: import.meta.env.VITE_HOST}
    });

    const webSocket = createWebSocket({
      plugins: [WebSocketReconnection],
      userModule: user
    });
    const chats = createChats({
      userModule: user,
      webSocketModule: webSocket
    });

    const devicesManager = createDevicesManager({
      config: {
        constraints: {
          video: {
            width: 720,
            height: 480,
            frameRate: 32
          },
          audio: {
            noiseSuppression: true,
            echoCancellation: true,
            autoGainControl: true
          }
        }
      }
    });

    const room = createRoom({
      webSocketModule: webSocket,
      userModule: user
    });

    const roomChat = createRoomChat({
      userModule: user,
      webSocketModule: webSocket,
      roomModule: room,
      chatsModule: chats,
    });

    const conference = createConference({
      userModule: user,
      webSocketModule: webSocket,
      devicesManagerModule: devicesManager!,
      roomModule: room,
      config: {
        maxBitrate: getCustomBitrate() != null ? getCustomBitrate()! : 600_000,
        codecs: ['vp9', 'vp8', 'av1']
      }
    });


    const reporter = new Reporter(
      {
        module: room,
        options: {
          logs: true,
          data: false,
          diffs: true
        }
      },
      ...conference ? [{
        module: conference,
        options: {
          logs: true,
          data: false,
          diffs: true
        }
      }] : [],
      ...devicesManager ? [{
        module: devicesManager,
        options: {
          logs: true,
          data: false,
          diffs: true
        }
      }] : []
    );

    reporter.onRecord(r => {
      console.log(convertBigInts(r.data));
    });

    setModules({chats, user, webSocket, devicesManager, room, roomChat, conference, reporter});

    const queryString = window.location.search;
    const query = new URLSearchParams(queryString);
    const roomId = query.get('room');
    const autoSendMessages = query.get('autosend');
    if (roomId) {
      setRoomId(roomId);
    }
    if (autoSendMessages) {
      setAutosend(true);
    }

    return () => {
      try {
        reporter.destroy()
        devicesManager?.destroy()
        webSocket.destroy();
        user.destroy();
        chats.destroy();
        room.destroy();
        roomChat.destroy();
        conference?.destroy();
      } catch (e: unknown) {
        if (e instanceof InstanceDestroyedError) return;
        throw e;
      }
    };
  }, []);

  const getToken = async () => {
    const result = await fetch(`${import.meta.env.VITE_API}/token`);
    const obj = await result.json();
    setToken(obj.token);
    return obj.token;
  };

  const createEmptyRoom = async () => {
    const result = await fetch(`${import.meta.env.VITE_API}/room`);
    const obj = await result.json();
    setRoomId(obj.room.id);
    return obj.room.id;
  };

  const openUser = () => {
    modules?.user.actions.open(token);
  };

  const openWebsocket = () => {
    modules?.webSocket.actions.open();
  };

  const joinRoom = () => {
    modules?.room.actions.joinRoom(roomId);
  };

  const activateDemo = async () => {
    const queryString = window.location.search;
    const query = new URLSearchParams(queryString);
    const roomId = query.get('room');

    const token = await getToken();
    await modules?.user.actions.open(token);
    await modules?.webSocket.actions.open();

    if (roomId) {
      await modules?.room.actions.joinRoom(roomId);
    } else {
      const newRoomId = await createEmptyRoom();
      await modules?.room.actions.joinRoom(newRoomId);
    }

    await modules?.devicesManager?.actions.turnOnCam().catch(() => {});
    await modules?.devicesManager?.actions.turnOnMic().catch(() => {});

    const sendMessageAtRandomIntervals = () => {
      const isSending = Math.random() >= 0.5;

      if (isSending) {
        modules?.roomChat?.actions.sendMessage(String(Date.now()));
      }
    };

    if (autosend) {
      setRandomInterval(sendMessageAtRandomIntervals, 1000, 5000);
    }
  };

  return <>

    <Container maxWidth="lg">
      <h1>Frontend ZDK demo</h1>
      <main>
        <Card style={{margin: '12px'}}>
          <div style={{border: 1}}></div>
          <Box
            display={'flex'}
            p={2}
          >
            <Typography sx={{color: 'text.secondary'}}>Do all steps using one button</Typography>
          </Box>
          <CardContent>
            <Box
              mt={3}
              display={'flex'}
              justifyContent={'center'}
            >
              <Button
                onClick={activateDemo}
                className={'primary'}
              >
                Activate Demo
              </Button>
            </Box>
          </CardContent>
        </Card>
        <div style={{textAlign: 'center'}}>or</div>
        <Card style={{margin: '12px'}}>
          <div style={{border: 1}}></div>
          <Box
            display={'flex'}
            p={2}>
            <Typography sx={{width: '6%', flexShrink: 0, minWidth: '32px'}}>
              {token ? <CheckCircleOutlineIcon style={{color: '#6ca141'}}/> : <PanoramaFishEyeIcon style={{color: 'grey'}}/>}
            </Typography>
            <Typography sx={{width: '12%', flexShrink: 0, minWidth: '56px'}}>Step 1</Typography>
            <Typography sx={{color: 'text.secondary'}}>Authenticate user with ZDK backend</Typography>
          </Box>
          <CardContent>
            <Typography
              variant="button"
              display="block"
              gutterBottom>
              Token
            </Typography>
            <Paper elevation={0}>
              <Typography
                p={1}
                style={{wordBreak: 'break-all'}}>
                {token}
              </Typography>
            </Paper>
            <Box
              mt={3}
              display={'flex'}
              justifyContent={'center'}>
              <Button
                onClick={getToken}>
                get Token
              </Button>
            </Box>
          </CardContent>
        </Card>
        <Card style={{margin: '12px'}}>
          <div style={{border: 1}}></div>
          <Box
            display={'flex'}
            p={2}>
            <Typography sx={{width: '6%', flexShrink: 0, minWidth: '32px'}}>
              {roomId ? <CheckCircleOutlineIcon style={{color: '#6ca141'}}/> : <PanoramaFishEyeIcon style={{color: 'grey'}}/>}
            </Typography>
            <Typography sx={{width: '12%', flexShrink: 0, minWidth: '56px'}}>Step 2</Typography>
            <Typography sx={{color: 'text.secondary'}}>Create room or use existing one</Typography>
          </Box>
          <CardContent>
            <Typography
              variant="button"
              display="block"
              gutterBottom>
              Room ID
            </Typography>
            <Paper elevation={0}>
              <Typography
                p={1}
                style={{wordBreak: 'break-all'}}>
                {roomId}
              </Typography>
            </Paper>
            <Box
              mt={3}
              display={'flex'}
              justifyContent={'center'}>
              <Button
                onClick={createEmptyRoom}>
                create new room
              </Button>
            </Box>
            <Typography
              p={1}
              style={{wordBreak: 'break-all'}}>
              or
            </Typography>
            <FormControl fullWidth>
              <InputLabel id="roomId">Enter the ID of the selected room</InputLabel>
              <Input
                id="demo-simple-select"
                value={undefined}
                onChange={(v) => setRoomId(v.target.value)}
              />
            </FormControl>

          </CardContent>
        </Card>

        <Card style={{margin: '12px'}}>
          <Box
            display={'flex'}
            p={2}>
            <Typography sx={{width: '6%', flexShrink: 0, minWidth: '32px'}}>
              {userModuleStatus === 'active' ? <CheckCircleOutlineIcon style={{color: '#6ca141'}}/> :
                <PanoramaFishEyeIcon style={{color: 'grey'}}/>}
            </Typography>
            <Typography sx={{width: '12%', flexShrink: 0, minWidth: '56px'}}>Step 3</Typography>
            <Typography sx={{color: 'text.secondary'}}>Received token use to authenticate ZDK (user module)</Typography>
          </Box>
          <CardContent>
            <Typography variant="button">
              user module status:&nbsp;
            </Typography>
            {userModuleStatus}
            <Box
              mt={3}
              display={'flex'}
              justifyContent={'center'}>
              <Button
                onClick={openUser}>
                authenticate ZDK
              </Button>
            </Box>
          </CardContent>
        </Card>

        <Card style={{margin: '12px'}}>
          <Box
            display={'flex'}
            p={2}>
            <Typography sx={{width: '6%', flexShrink: 0, minWidth: '32px'}}>
              {webSocketModuleStatus === 'connected' ? <CheckCircleOutlineIcon style={{color: '#6ca141'}}/> :
                <PanoramaFishEyeIcon style={{color: 'grey'}}/>}
            </Typography>
            <Typography sx={{width: '12%', flexShrink: 0, minWidth: '56px'}}>Step 4</Typography>
            <Typography sx={{color: 'text.secondary'}}>Start connection with WebSocket (webSocket module)</Typography>
          </Box>
          <CardContent>
            <Typography variant="button">
              webSocket module status:&nbsp;
            </Typography>
            {webSocketModuleStatus}
            <Box
              mt={3}
              display={'flex'}
              justifyContent={'center'}>
              <Button
                onClick={openWebsocket}>
                connect with Websocket
              </Button>
            </Box>
          </CardContent>
        </Card>

        <Card style={{margin: '12px'}}>
          <Box
            display={'flex'}
            p={2}>
            <Typography sx={{width: '6%', flexShrink: 0, minWidth: '32px'}}>
              {roomModuleStatus === 'ready' ? <CheckCircleOutlineIcon style={{color: '#6ca141'}}/> :
                <PanoramaFishEyeIcon style={{color: 'grey'}}/>}
            </Typography>
            <Typography sx={{width: '12%', flexShrink: 0, minWidth: '56px'}}>Step 5</Typography>
            <Typography sx={{color: 'text.secondary'}}>Join to a room (room module)</Typography>
          </Box>
          <CardContent>
            <Typography variant="button">
              room module status:&nbsp;
            </Typography>
            {roomModuleStatus}
            <Box
              mt={3}
              display={'flex'}
              justifyContent={'center'}>
              <Button
                onClick={joinRoom}>
                enter room
              </Button>
            </Box>
          </CardContent>
        </Card>


        {modules?.devicesManager && <DevicesManagerComponent devicesManager={modules.devicesManager}/>}
        {modules?.conference && modules?.room && roomModuleStatus && roomModuleStatus === 'ready' && <ConferenceComponent
          user={modules.user}
          devicesManager={modules.devicesManager!}
          room={modules.room}
          conference={modules.conference}/>}

        {modules?.roomChat && roomModuleStatus === 'ready' && <RoomChat roomChat={modules?.roomChat}/>}

      </main>

    </Container>
    x
  </>;
};
