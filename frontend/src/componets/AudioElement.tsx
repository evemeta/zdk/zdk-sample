import {memo, useEffect, useRef} from 'react';


interface MediaElementProps {
  mediaStream: MediaStream | undefined,
  muted: boolean,
  volume: number
}

export const AudioElement = memo(({mediaStream, muted, volume}: MediaElementProps) => {
  const audioRef = useRef<HTMLAudioElement>(null);

  useEffect(() => {
    if (audioRef.current) {
      audioRef.current.srcObject = mediaStream || null;
    }

  }, [mediaStream]);

  useEffect(() => {
    if (audioRef.current) {
      audioRef.current.muted = muted;
    }
  }, [muted]);

  useEffect(() => {
    if (audioRef.current) {
      audioRef.current.volume = volume;
    }
  }, [volume]);

  return <audio
    ref={audioRef}
    controls={false}
    autoPlay
    playsInline
    muted={muted}
  />;
});

