import React, {FC, Fragment, useState} from 'react';
import {IRoomChat} from '@zdk/room_chat_module';
import {IMessage} from '@zdk/chats';
import {useGetter} from '../utils/useGetter.ts';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import CardContent from '@mui/material/CardContent';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

export const RoomChat: FC<{ roomChat: IRoomChat }> = ({roomChat}) => {
  const roomChatMessages = useGetter(roomChat.data.roomChatMessages);
  const roomChatStatus = useGetter(roomChat.data.roomChatStatus);
  const roomChatMembersAmount = useGetter(roomChat.data.roomChatMembersAmount);

  const [msg, setMsg] = useState('a message');
  const [msgToEdit, setMsgToEdit] = useState<IMessage>();

  const bigintToNumber = (n?: bigint) => {
    if (!n) return undefined;
    if (n === BigInt(0)) return undefined;
    return Number(n);
  };

  const renderStatus = (msg: IMessage) => {
    const getDeleteStatus = () => {
      switch (msg.deleteStatus) {
        case 'loading': {
          return <span style={{color: 'yellow'}}>deleting</span>;
        }
        case 'interrupted': {
          return <span style={{color: 'red'}}>delete failed</span>;
        }
      }
    };
    switch (msg.sendingStatus) {
      case 'ready': {
        return <>
          <span style={{color: 'lime'}}>{msg.sendingStatus}</span>
          {getDeleteStatus()}</>;
      }
      case 'loading': {
        return <>
          <span style={{color: 'yellow'}}>{msg.sendingStatus}</span>
          {getDeleteStatus()}</>;
      }
      case 'interrupted': {
        return <>
          <span style={{color: 'red'}}>{msg.sendingStatus}</span>
          {getDeleteStatus()}</>;
      }
    }
  };

  const bigintToTime = (n?: bigint) => {
    if (!n) return undefined;
    if (n === BigInt(0)) return undefined;
    const date = new Date(Number(n));

    function toString(number: number, padLength: number) {
      return number.toString().padStart(padLength, '0');
    }

    return toString(date.getHours(), 2)
      + ':' + toString(date.getMinutes(), 2)
      + ':' + toString(date.getSeconds(), 2)
      + '.' + toString(date.getMilliseconds(), 3);
  };

  const renderMsg = (msg: IMessage) =>
    <Fragment key={msg.id}>

      <Paper
        elevation={3}
        style={{marginBottom: 6}}>
        <CardContent>
          <Stack
            direction="row"
            justifyContent={'space-between'}
            spacing={4}
            flexWrap={'wrap'}
            mt={1}>
            <Typography variant="body2">{msg.content}</Typography>
            <Stack
              direction="row"
              spacing={4}>
              <Button
                size="small"
                disabled={msg.sendingStatus !== 'interrupted'}
                onClick={() => resendMessage(msg)}>resend</Button>
              <Button
                size="small"
                onClick={() => deleteMessage(msg)}>delete</Button>
              <Button
                size="small"
                onClick={() => setMsgToEdit(msg)}>edit</Button>
            </Stack>
          </Stack>
          <Stack
            direction="row"
            spacing={4}
            mt={1}>
            <Typography
              sx={{fontSize: 12}}
              color="text.disabled"
              gutterBottom>
              createTime: {bigintToTime(msg.createTime)}
            </Typography>
            <Typography
              sx={{fontSize: 12}}
              color="text.disabled"
              gutterBottom>
              updateTime: {bigintToTime(msg.updateTime)}
            </Typography>
            <Typography
              sx={{fontSize: 12}}
              color="text.secondary"
              gutterBottom>
              status: {renderStatus(msg)}
            </Typography>
          </Stack>
          <Stack
            direction="row"
            spacing={2}>
            <Typography
              sx={{fontSize: 12}}
              color="text.disabled"
              gutterBottom>
              messageId: {msg.id}
            </Typography>
            <Typography
              sx={{fontSize: 12}}
              color="text.disabled"
              gutterBottom>
              chatId: {msg.chatId}
            </Typography>
          </Stack>
        </CardContent>
      </Paper>
    </Fragment>;

  const sendMsg = () => {
    roomChat.actions.sendMessage(msg).then(r => {
      // console.log('ok');
    }).catch(err => {
      console.error(err);
    });
  };

  const deleteMessage = (msg: IMessage) => {
    roomChat.actions.deleteMessage(msg.id).then(r => {
      console.log('ok');
    }).catch(err => {
      console.error(err);
    });
  };

  const resendMessage = (msg: IMessage) => {
    roomChat.actions.resendMessage(msg.id).then(r => {
      console.log('ok');
    }).catch(err => {
      console.error(err);
    });
  };

  const updateMessage = () => {
    roomChat.actions.updateMessage(msgToEdit!.id, msgToEdit!.content).then(r => {
      console.log('ok');
    }).catch(err => {
      console.error(err);
    });
  };

  return <Paper
    elevation={1}
    style={{
      padding: '12px',
      margin: '12px',
      background: (roomChatStatus === 'loading' ? '#ffff0021' : undefined)
    }}>
    <h3>RoomChat</h3>
    <div>
      <Typography variant="button">
        Chat Status:&nbsp;
      </Typography>
      {roomChatStatus}
    </div>
    <div>
      <Typography variant="button">
        Chat Members Amount:&nbsp;
      </Typography>
      {bigintToNumber(roomChatMembersAmount)}
    </div>

    <div style={{marginTop: 8, marginBottom: 4}}>
      <div style={{marginTop: 8, marginBottom: 4}}>
        <Typography
          variant="button">
          Chat Messages:&nbsp;
        </Typography>
      </div>
      {roomChatMessages?.map((e, i, arr) => renderMsg(e))}
    </div>

    <Stack gap={2}>
      <div style={{marginTop: 16}}>
        <Stack
          direction={'row'}
          justifyContent={'flex-end'}>
          <TextField
            fullWidth
            id="filled-basic"
            label="message"
            variant="outlined"
            value={msg}
            onChange={v => setMsg(v.target.value)}/>
          <Button
            style={{minWidth: 200}}
            onClick={sendMsg}>send message</Button>
        </Stack>
      </div>

      {msgToEdit && <div>
        <Stack
          direction={'row'}
          justifyContent={'flex-end'}>
          <TextField
            fullWidth
            id="filled-basic"
            label="edit message"
            variant="outlined"
            value={msgToEdit.content}
            onChange={v => setMsgToEdit((msg) => ({...msg!, content: v.target.value}))}/>
          <Button
            style={{minWidth: 100}}
            onClick={updateMessage}>confirm</Button>
          <Button
            color="warning"
            style={{minWidth: 100}}
            onClick={() => setMsgToEdit(undefined)}>cancel</Button>
        </Stack>
      </div>
      }
    </Stack>


  </Paper>;
};
