import {useEffect, useRef} from 'react';


interface MediaElementProps {
  mediaStream: MediaStream
}

export const MediaElement = ({mediaStream}: MediaElementProps) => {
  const divRef = useRef<HTMLDivElement>(null);
  const videoRef = useRef<HTMLVideoElement>(null);


  useEffect(() => {
    videoRef.current!.srcObject = mediaStream;
  }, [mediaStream]);

  return (
    <div ref={divRef}>
      <video width="100%" height="100%" ref={videoRef} autoPlay muted playsInline>
      </video>
    </div>
  );
};

