import {defineConfig} from 'vite';
import react from '@vitejs/plugin-react'
import dotenv from 'dotenv';

// https://vitejs.dev/config/
export default defineConfig(({mode, command}) => {
  const envPath = `.env.${mode}${command === 'build' ? '': '.local'}`;
  dotenv.config({ path: envPath });

  return ({
    build: {
      outDir: './build',
      rollupOptions: {
        output: {
          assetFileNames: () => {
            return `static/[name]-[hash][extname]`
          },
          chunkFileNames: 'static/js/[name]-[hash].js',
          entryFileNames: 'static/js/[name]-[hash].js',
        }
      }
    },
    plugins: [react()],
  })
})
