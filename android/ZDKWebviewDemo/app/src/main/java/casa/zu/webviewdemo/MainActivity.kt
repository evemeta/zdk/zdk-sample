package casa.zu.webviewdemo

import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.webkit.PermissionRequest
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.OptIn
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.datasource.DataSource
import androidx.media3.datasource.DefaultHttpDataSource
import androidx.media3.exoplayer.hls.HlsMediaSource
import androidx.media3.exoplayer.source.MediaSource
import androidx.media3.exoplayer.source.ProgressiveMediaSource
import androidx.media3.ui.PlayerView
import casa.zu.webviewdemo.ui.theme.ZDKWebviewDemoTheme

class MainActivity : ComponentActivity() {
    private var player: ExoPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ZDKWebviewDemoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column {
                        Greeting("ZDK TEST")
                        VideoView(mediaUrl = "https://cdn.zu.casa/zeetv/SampleVideo_1280x720_10mb.mp4")
                        loadWebUrl(url = "https://demo.zdk.zu.casa/?room=1d6951b2-19dd-45da-9a4d-b185b189d752")
//                        loadWebUrl(url = "https://webrtc.github.io/samples/src/content/devices/input-output/")
                    }

                }
            }
        }
    }

    private val playerListener = object : Player.Listener {
        override fun onPlaybackStateChanged(playbackState: Int) {
            if(playbackState == ExoPlayer.STATE_READY){
                player?.play()
            }
            val stateString: String = when (playbackState) {
                ExoPlayer.STATE_IDLE -> "ExoPlayer.STATE_IDLE      -"
                ExoPlayer.STATE_BUFFERING -> "ExoPlayer.STATE_BUFFERING -"
                ExoPlayer.STATE_READY -> {
                    "ExoPlayer.STATE_READY     -"
                }
                ExoPlayer.STATE_ENDED -> "ExoPlayer.STATE_ENDED     -"
                else -> "UNKNOWN_STATE             -"
            }
            Log.d("EXO", "changed state to $stateString")
        }
    }

    @OptIn(UnstableApi::class) @Composable
    fun VideoView(mediaUrl: String) {
        this@MainActivity.player = ExoPlayer.Builder(this)
            .build()
            .apply {
                val source = if (mediaUrl.contains("m3u8"))
                    HlsMediaSource.Factory(DefaultHttpDataSource.Factory()).createMediaSource(MediaItem.fromUri(mediaUrl))
                else
                    ProgressiveMediaSource.Factory(DefaultHttpDataSource.Factory()).createMediaSource(MediaItem.fromUri(Uri.parse(mediaUrl)))

                setMediaSource(source)
                prepare()
                addListener(playerListener)
            }
        AndroidView(
            factory = {
                PlayerView(this@MainActivity).apply {
                    this.player = this@MainActivity.player
                    layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                }
            }
        )
    }

    @Composable
    fun loadWebUrl(url: String) {
        AndroidView(factory = {
            WebView(this).apply {
                webChromeClient = object : WebChromeClient() {
                    override fun onPermissionRequest(request: PermissionRequest) {
                        request.grant(request.resources)
                    }
                }

                val requiredPermissions = mutableListOf<String>()
                if (ContextCompat.checkSelfPermission(this@MainActivity, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    requiredPermissions.add(android.Manifest.permission.RECORD_AUDIO)
                }

                if (ContextCompat.checkSelfPermission(this@MainActivity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requiredPermissions.add(android.Manifest.permission.CAMERA)
                }

                if(requiredPermissions.isNotEmpty()){
                    ActivityCompat.requestPermissions(this@MainActivity, requiredPermissions.toTypedArray(), 1111);
                }
//
//                if (ContextCompat.checkSelfPermission(this@MainActivity, android.Manifest.permission.MODIFY_AUDIO_SETTINGS) != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(this@MainActivity, arrayOf(android.Manifest.permission.MODIFY_AUDIO_SETTINGS), 3333);
//                }
                settings.javaScriptEnabled = true
                settings.setNeedInitialFocus(false)
                settings.mediaPlaybackRequiresUserGesture = false
                loadUrl(url)
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            Log.d("MainActivity", "Permission granted ${permissions.joinToString()}")
            // permission was granted, yay!
        } else {
            // permission denied, boo!
            Log.d("MainActivity", "Permission denied")
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}


@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    ZDKWebviewDemoTheme {
        Greeting("Android")
    }
}