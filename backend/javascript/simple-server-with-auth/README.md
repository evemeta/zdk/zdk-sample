# simple-server-with-auth (javascript)

This example builds on top of the `simple-server` providing a simple in-memory authentication to make sure that the `/api/me` endpoint can be executed only by real users.

Once again this example is not meant to be used in production, this sample provides a more detailed idea how getting a token should be implemented for a real world scenario.

Instructions to prepare:
```shell
npm install
```

Instructions to run
```shell
export ZDK_API_KEY="<your_api_key>"
npm run server
```

This example requires node.js and NPM.

Then open in your browser http://localhost:3000/