const express = require('express')
const app = express()

const hash = require('pbkdf2-password')();
const path = require('path');
const session = require('express-session');
const crypto = require('crypto');
const fetch = require("node-fetch");
const ZDK_API_HOST = "dev.zu.casa";

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// middleware
app.use(express.urlencoded({extended: false}))
app.use(session({
    resave: false, // don't save session if unmodified
    saveUninitialized: false, // don't create session until something stored
    secret: 'g45ctf3cf2J^w'
}));

require('dotenv').config();

const ZDK_API_KEY = process.env.ZDK_API_KEY;
if (!ZDK_API_KEY) {
    console.log("ZDK_API_KEY env variable must be defined!")
    process.exit(99);
}

// Session-persisted message middleware
app.use(function (req, res, next) {
    var err = req.session.error;
    var msg = req.session.success;
    delete req.session.error;
    delete req.session.success;
    res.locals.message = '';
    if (err) res.locals.message = '<p class="msg error">' + err + '</p>';
    if (msg) res.locals.message = '<p class="msg success">' + msg + '</p>';
    next();
});

// dummy database
var users = {
    sample_user: {id: "5896f971-59f0-49b0-b358-c3596f169635", name: 'sample_user'}
};


// when you create a user, generate a salt
// and hash the password ('test123' is the pass here)
hash({password: 'test123'}, function (err, pass, salt, hash) {
    if (err) throw err;
    // store the salt & hash in the "db"
    users.sample_user.salt = salt;
    users.sample_user.hash = hash;
});


//Authenticate user with ZDK backend
async function createAuthToken(id, nickname) {
    const opts = {
        method: "POST",
        headers: {
            'content-type': 'application/json',
            'authorization': 'Bearer ' + ZDK_API_KEY,
        },
        body: JSON.stringify({
            arguments: [
                {
                    id: id,
                    avatar: "", //optional
                    nickname: nickname, //optional
                    fullname: "", //optional
                    permissions: [100, 200, 300, 400, 500, 600, 700, 800]
                }
            ]
        })
    }

    const result = await fetch('https://user.' + ZDK_API_HOST + '/user.tokens.private.v1.Service/Create', opts);
    return result.json();
}

async function createRoom() {
    const opts = {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'Authorization':  'Bearer ' + ZDK_API_KEY,
        },
        body: JSON.stringify({
            arguments: [
                {
                    metadata: {name: 'test room'},
                    kind: 2,
                    retention: 2
                }
            ]
        })
    };

    const result = await fetch('https://room.' + ZDK_API_HOST + '/room.rooms.private.v1.Service/Create', opts);
    return result.json();
}

// Authenticate using our plain-object database of doom!
function authenticate(name, pass, fn) {
    if (!module.parent) console.log('authenticating %s:%s', name, pass);
    var user = users[name];
    // query the db for the given username
    if (!user) return fn(null, null)
    // apply the same algorithm to the POSTed password, applying
    // the hash against the pass / salt, if there is a match we
    // found the user
    hash({password: pass, salt: user.salt}, function (err, pass, salt, hash) {
        if (err) return fn(err);
        if (hash === user.hash) return fn(null, user)
        fn(null, null)
    });
}

function restrict(req, res, next) {
    if (req.session.user) {
        next();
    } else {
        req.session.error = 'Access denied!';
        res.redirect('/login');
    }
}

app.use('/static', express.static(path.join(__dirname, '/../../../frontend/build/static')))

app.get('/', function (req, res) {
    if (req.session.user) {
        res.sendFile(path.join(__dirname, '/../../../frontend/build/index.html'));
    } else {
        res.redirect('/login');
    }
});

app.get('/restricted', restrict, function (req, res) {
    res.send('Wahoo! restricted area, click to <a href="/logout">logout</a>');
});

app.get('/logout', function (req, res) {
    // destroy the user's session to log them out
    // will be re-created next request
    req.session.destroy(function () {
        res.redirect('/');
    });
});

app.get('/login', function (req, res) {
    if (req.session.user) {
        res.redirect('/');
    } else {
        res.render('login');
    }

});

app.get('/api/me', function (req, res) {
    if (!req.session.user) {
        res.sendStatus(403);
        return;
    }

    res.send(JSON.stringify({id: req.session.user.id, name: req.session.user.name}));
});

app.get('/api/token', async function (req, res) {
    if (!req.session.user) {
        res.sendStatus(403);
        return;
    }

    const result = await createAuthToken(req.session.user.id, req.session.user.name)
    res.send(JSON.stringify({token: result.tokens[0]}));
});

app.get('/api/room', async function (req, res) {
    if (!req.session.user) {
        res.sendStatus(403);
        return;
    }

    const result = await createRoom();
    res.send(JSON.stringify({room: result.rooms[0]}));
});

app.get('/register', function (req, res) {
    if (req.session.user) {
        res.redirect('/');
        return;
    }

    res.render('register');
});

app.post('/register', function (req, res, next) {
    var user = users[req.body.username];
    if (user) {
        req.session.error = 'Registration failed, user already exists!';
        res.redirect('/register');
        return
    }

    user = {id: crypto.randomUUID(), name: req.body.username};
    hash({password: req.body.password}, function (err, pass, salt, hash) {
        if (err) throw err;
        // store the salt & hash in the "db"
        user.salt = salt;
        user.hash = hash;
        users[req.body.username] = user;

        req.session.regenerate(function () {
            // Store the user's primary key
            // in the session store to be retrieved,
            // or in this case the entire user object
            req.session.user = user;
            req.session.success = 'Authenticated as ' + user.name
                + ' click to <a href="/logout">logout</a>. '
                + ' You may now access <a href="/restricted">/restricted</a>.';
            res.redirect('back');
        });
    });


});
app.post('/login', function (req, res, next) {
    authenticate(req.body.username, req.body.password, function (err, user) {
        if (err) return next(err);
        if (user) {
            // Regenerate session when signing in
            // to prevent fixation
            req.session.regenerate(function () {
                // Store the user's primary key
                // in the session store to be retrieved,
                // or in this case the entire user object
                req.session.user = user;
                req.session.success = 'Authenticated as ' + user.name
                    + ' click to <a href="/logout">logout</a>. '
                    + ' You may now access <a href="/restricted">/restricted</a>.';
                res.redirect('back');
            });
        } else {
            req.session.error = 'Authentication failed, please check your '
                + ' username and password.'
                + ' (use "sample_user" and "test123")';
            res.redirect('/login');
        }
    });
});

/* istanbul ignore next */
if (!module.parent) {
    app.listen(3000);
    console.log('Express started on port 3000');
}