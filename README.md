# zdk-sample (Sample Project)

This repository contains a sample implementation of ZDK SDK for both frontend and backend.

# running demo
frontend + backend/go/simple-server

https://demo.zdk.zu.casa/

# Deployment to https://demo.zdk.zu.casa/

`NAMESPACE=dev make build`

then:

`NAMESPACE=dev make deploy`
